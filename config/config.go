package config

import (
	"encoding/base64"
	"log"
	"os"
	"reflect"
	"strconv"
	"strings"

	"gopkg.in/yaml.v2"
)

func Load() (conf *Config, err error) {

	configFilePath := "/assets/config.yaml"

	if encConfPath := os.Getenv(ConfigFilePathEnvKey); encConfPath != "" {
		configFilePath = encConfPath
	}

	err = conf.loadYamlFile(configFilePath)
	if err != nil {
		return
	}

	err = updateConfigFromEnv(&conf)
	if err != nil {
		return
	}

	err = parseBase64Values(&conf)
	if err != nil {
		return
	}

	return
}

func (conf *Config) loadYamlFile(filePath string) error {

	if _, err := os.Stat(filePath); err != nil {
		if wd, er := os.Getwd(); er == nil {
			log.Println("config file not found from: ", wd)
		} else {
			log.Println("config file not found")
		}

		return err
	}

	fileData, err := os.ReadFile(filePath)
	if err != nil {
		log.Println("failed to read config file err= ", err.Error())
		return err
	}

	if err := yaml.Unmarshal(fileData, conf); err != nil {
		log.Println("yaml unmarshal err= ", err.Error())
		return err
	}

	return nil
}

func updateConfigFromEnv(obj interface{}) error {

	objVal := reflect.ValueOf(obj).Elem()

	objType := objVal.Type()

	for i := 0; i < objVal.NumField(); i++ {

		field := objVal.Field(i)

		tagVal := objType.Field(i).Tag.Get("envKey")

		if tagVal != "" {
			envValue := os.Getenv(tagVal)

			if envValue == "" {
				continue
			}
			switch field.Kind() {

			case reflect.String:
				field.SetString(envValue)
			case reflect.Bool:
				field.SetBool(envValue == "true")

			case reflect.Int, reflect.Int8, reflect.Int32, reflect.Int64:
				val, err := strconv.ParseInt(envValue, 10, 64)
				if err != nil {
					return err
				}

				field.SetInt(val)

			case reflect.Float32, reflect.Float64:
				val, err := strconv.ParseFloat(envValue, 64)
				if err != nil {
					return err
				}

				field.SetFloat(val)

			case reflect.Slice:

				if field.Type().Elem().Kind() == reflect.String {
					values := strings.Split(envValue, ",")
					field.Set(reflect.ValueOf(values))
				}

			default:
				log.Printf("\nenv field type is not handled envValue= %v, type =%v\n", envValue, field.Type())
			}
		}

	}

	return nil
}

func parseBase64Values(obj any) error {

	objVal := reflect.ValueOf(obj).Elem()

	objType := objVal.Type()

	for i := 0; i < objVal.NumField(); i++ {

		field := objVal.Field(i)

		tagVal := objType.Field(i).Tag.Get("bs64")

		if tagVal == "true" {
			decodedVal, err := Base64Decode(field.String())
			if err != nil {
				return err
			}

			field.SetString(decodedVal)

		} else if field.Kind() == reflect.Struct {
			if err := parseBase64Values(field.Addr().Interface()); err != nil {
				return err
			}
		}
	}

	return nil
}

func Base64Decode(s string) (string, error) {
	if data, err := base64.StdEncoding.DecodeString(s); err != nil {
		return "", err
	} else {
		return string(data), nil
	}
}
