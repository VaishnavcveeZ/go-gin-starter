package config

const (
	EnvKeyPrefix = "APP_"
)

type Config struct {
	Version  string
	Server   Server
	Database Database
}

type Server struct {
	Port string `env:"SERVER_PORT"`
}

type Database struct {
	Name     string `env:"DATABASE_PORT"`
	Port     string `env:"DATABASE_PORT"`
	UserName string `env:"DATABASE_USERNAME"`
	Password string `env:"DATABASE_PASSWORD" bs64:"true"`
	Timezone string `env:"DATABASE_TIMEZONE"`
}
