package database

type DbInterface interface {
	Init() error
	Ping() error
	Close() error
}
