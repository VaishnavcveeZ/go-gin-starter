package logger

import (
	"fmt"
	"io"
	"net/http"
	"runtime"
	"strings"
	"telemedicine/services/db"
	"telemedicine/services/models"

	"os"
	"path/filepath"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"gopkg.in/natefinch/lumberjack.v2"

	"github.com/sirupsen/logrus"
)

var logrusLogger = logrus.New()

var logFile *os.File
var logFilePath string
var logFileName string

var logDataBase db.DbInterface
var enableLoggerDb bool

// InitLogger- Initialise logg file details
func InitLogger(filePath, fileName string, maxSize, maxBackups, maxAge int, compress, logStdOut bool) {

	logFileName = fileName
	logFilePath = filePath

	logrusLogger.SetFormatter(&logrus.JSONFormatter{})

	logrusLogger.SetLevel(logrus.TraceLevel)
	if logStdOut {
		// for lambda we can get console logs in cloud watch. or we can set up hook.
		logrusLogger.SetOutput(os.Stdout)
	} else {
		logrusLogger.SetOutput(&lumberjack.Logger{
			Filename:   filepath.Join(logFilePath, logFileName),
			MaxSize:    maxSize, // megabytes
			MaxBackups: maxBackups,
			MaxAge:     maxAge, //days
			Compress:   compress,
		})
	}
}

func ClearLogData() {
	err := os.Truncate(filepath.Join(logFilePath, logFileName), 0)
	if err != nil {
		logrusLogger.Println("failed to clear log")
	}
}

func PushErrorToDb(database db.DbInterface) {
	logDataBase = database
	enableLoggerDb = true
}

func GetWritter() io.Writer {
	return logrusLogger.Out
}

func Close() {
	logDataBase = nil
	logFile.Close()
}

// getFileAndLine - to get log invoking file and code line.
func getFileAndLine(data *map[string]interface{}, l *logrus.Fields, callerSkip ...int) {

	callerSkipValue := 3
	if len(callerSkip) > 0 {
		callerSkipValue = callerSkip[0]
	}

	// get the log invoked line from our code.
	// we use Caller(3) because the actual log invoked code is 3 level before this line
	_, file, line, ok := runtime.Caller(callerSkipValue)
	if ok {
		f := strings.Split(file, "/")
		if len(f) > 2 {
			(*l)["file"] = fmt.Sprintf("%s/%s", f[len(f)-2], f[len(f)-1])
			(*data)["file"] = fmt.Sprintf("%s/%s", f[len(f)-2], f[len(f)-1])

		} else if len(f) > 1 {
			(*l)["file"] = f[len(f)-1]
			(*data)["file"] = f[len(f)-1]

		}

		(*l)["line"] = line
		(*data)["line"] = line

	}
}

// getRequestIP - get user ip from request context
func getRequestIP(c *gin.Context) (ip string) {

	if i, ok := c.RemoteIP(); ok {
		ip = i.String()
	}
	return
}

// getRequestId - get request id from request context or we add a new request id
//
//	this will help to track api path if we use this context to call other microservices
func getRequestId(c *gin.Context) (reqId string) {
	reqId = c.Request.Header.Get("requestid")
	if reqId == "" {
		reqId = "req-" + uuid.New().String()
		c.Request.Header.Set("requestid", reqId)
	}
	return
}

// getAdditionalDetails - include addition details of the request in log field.
func getAdditionalDetails(c *gin.Context, l *logrus.Fields, callerSkip ...int) map[string]interface{} {

	var data = make(map[string]interface{})

	if c != nil {
		ip := getRequestIP(c)
		(*l)["ip"] = getRequestIP(c)
		data["ip"] = ip

		requestId := getRequestId(c)
		(*l)["request_id"] = requestId
		data["request_id"] = requestId
	}

	getFileAndLine(&data, l, callerSkip...)

	return data
}

// ApiStartLog - used to log at the time of an api request start. impliment using middleware
func ApiStartLog(c *gin.Context) {

	l := logrus.Fields{
		"method":   c.Request.Method,
		"url":      c.Request.URL.String(),
		"protocol": c.Request.Proto,
		"referer":  c.Request.Referer(),
	}
	getAdditionalDetails(c, &l)
	logrusLogger.WithFields(l).Infof("api request started")
}

// ApiCompletedLog - used to log at the time of an api complete the response. impliment using middleware
func ApiCompletedLog(c *gin.Context, latency int64) {
	l := logrus.Fields{
		"status_code": c.Writer.Status(),
		"latency_ms":  latency,
	}

	getAdditionalDetails(c, &l)
	logrusLogger.WithFields(l).Infof("api request completed")
}

func Error(c *gin.Context, msg string, args ...interface{}) {
	l := logrus.Fields{}

	data := getAdditionalDetails(c, &l)
	logrusLogger.WithFields(l).Error(fmt.Sprintf(msg, args...))

	// send log data to db
	if enableLoggerDb && logDataBase != nil {

		logDataBase.InsertLoggerData(models.LoggerDetailsModel{
			RequestId:  getRequestId(c),
			LogMessage: fmt.Sprintf(msg, args...),
			LogDetails: func() string {
				if data != nil {
					return fmt.Sprintf("%+v", data)
				}
				return ""
			}(),
		})
	}

}

func Debug(c *gin.Context, msg string, args ...interface{}) {
	l := logrus.Fields{}

	data := getAdditionalDetails(c, &l)
	logrusLogger.WithFields(l).Debug(fmt.Sprintf(msg, args...))

	if enableLoggerDb && logDataBase != nil {

		logDataBase.InsertLoggerData(models.LoggerDetailsModel{
			RequestId:  getRequestId(c),
			LogMessage: fmt.Sprintf(msg, args...),
			LogDetails: func() string {
				if data != nil {
					return fmt.Sprintf("%+v", data)
				}
				return ""
			}(),
		})
	}
}

func Info(c *gin.Context, msg string, args ...interface{}) {
	l := logrus.Fields{}
	logrusLogger.WithFields(l).Info(fmt.Sprintf(msg, args...))
}

func ErrorWithoutCtx(msg string, args ...interface{}) {
	l := logrus.Fields{}

	getAdditionalDetails(nil, &l)
	logrusLogger.WithFields(l).Errorf(msg, args...)
}

func DebugWithoutCtx(msg string, args ...interface{}) {
	l := logrus.Fields{}

	getAdditionalDetails(nil, &l)
	logrusLogger.WithFields(l).Debugf(msg, args...)
}

func InfoWithoutCtx(msg string, args ...interface{}) {
	l := logrus.Fields{}

	logrusLogger.WithFields(l).Infof(msg, args...)
}

func panicError(c *gin.Context, msg string, args ...interface{}) {
	l := logrus.Fields{}

	getAdditionalDetails(c, &l, 7)
	logrusLogger.WithFields(l).Error(fmt.Sprintf(msg, args...))

	getAdditionalDetails(c, &l, 8)
	logrusLogger.WithFields(l).Error(fmt.Sprintf(msg, args...))
}

func RecoveryFunc(c *gin.Context, err interface{}) {
	if err != nil {
		// write panic log
		panicError(c, "go panic err= %v", err)
	}

	if c != nil {
		// return error response
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "error",
			"code":    http.StatusInternalServerError,
			"message": "internal server error",
		})
	}
}
