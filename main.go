package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func main() {
	router := gin.New()

	router.GET("/status", Status)

	router.Run(":5000")

}

func Status(c *gin.Context) {
	c.JSON(http.StatusOK, "ok")
}
