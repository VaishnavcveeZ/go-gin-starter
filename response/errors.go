package response

const (
	InternalServerError = "Internal server error"
	InvalidRequest      = "Invalid request"
)

var InternalServerErrorResponse = response{
	Status:  StatusFailed,
	Message: InternalServerError,
}
