package response

import "github.com/gin-gonic/gin"

type Status string

const (
	StatusSuccess Status = "success"
	StatusFailed  Status = "failed"
)

type response struct {
	Status  Status      `json:"status"`
	Message string      `json:"message,omitempty"`
	Data    interface{} `json:"data,omitempty"`
}

// Response Writer
type ResponseWriter struct {
	status  Status
	message string
	data    interface{}
	ctx     *gin.Context
}
