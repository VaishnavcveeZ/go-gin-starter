package response

import (
	"log"

	"github.com/gin-gonic/gin"
)

func NewResponse(ctx *gin.Context) *ResponseWriter {
	if ctx.Err() != nil {
		return nil
	}
	return &ResponseWriter{
		ctx: ctx,
	}
}

func (r *ResponseWriter) AttachData(data interface{}) {
	if data == nil {
		return
	}
	r.data = data
}

func (r *ResponseWriter) SendSuccess(code int, message string) {
	r.status = StatusSuccess
	r.ctx.SecureJSON(code, r.apiResponse())
}

func (r *ResponseWriter) SendFailure(code int, message string, logError ...string) {
	log.Printf("Error: %v", logError)
	r.status = StatusFailed
	r.ctx.SecureJSON(code, r.apiResponse())
}

func (r *ResponseWriter) apiResponse() response {

	if r.status == "" || (r.message == "" && r.data == nil) {
		return InternalServerErrorResponse
	}

	var resp response
	resp.Status = r.status
	resp.Message = r.message

	if r.data != nil {
		resp.Data = r.data
	}
	return resp
}
